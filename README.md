# HyperData

## What is HyperData?

HyperData is a simple hypermedia format for linking serialised objects together. It enables 
client-side scripting within objects for data transformation and conditional representation. 
It also supports output templating.

## Goals

- Simplify linking between serialised objects without the complexity of schemas and namespaces
- Standardise and encode logic within objects to simplify clients and ensure consistency
across multiple clients

## Components

- HyperData Object: any serialised object.
- HyperData Client: A program designed to consume HyperData objects according to this specification.
- HyperData Server: Anything that can serve out serialised objects – e.g. your local machine, a CDN or cloud server.

## Summary

### Linking Objects

As with a HyperText document, a HyperData object can hyperlink to another object by specifying 
a target URI as a hypermedia reference (`href`). A hyperlink is specified using the key `@L`:

```json
{
  "@n" : 1,
  "foo" : "bar",
  "@L" : {
    "href" : "example.json",
    "name" : "Example Object"
  }
}
```

The key `@n` indicates the HyperData Version Number, this key is not present in the output object.
For&nbsp;more information about linking, get into the [detail of linking](#linking-objects-detailed).

### Scripting

HyperData uses the excellent [Data Transformation Language (DTL)](https://www.getdtl.org)
for scripting within objects, this enables conditional representation among many other features. 
For example, the following HyperData object uses a conditional expression to return a different object 
based on input data provided by the client – a user's language:

```json
{
  "@n" : 1,
  "name" : "Example Co",
  "support_number" : "{ ?( $language == 'DE' '+49123456789' '+1555123456' ) }"
}
```

For a German speaking user, the resulting output object would be:

```json
{
  "name" : "Example Co",
  "support_number" : "+49123456789"
}
```

For other users, the resulting output object is:

```json
{
  "name" : "Example Co",
  "support_number" : "+1555123456"
}
```
For more information get into the [detail of scripting](#scripting-detailed).

### Output Templating

HyperData includes the concept of output templating, this enables a HyperData client to take the
raw HyperData object and render it in a format to suit the purposes of the HyperData client or 
its user.

For example, one client could render the above example object as a 
[Schema.org Organization](https://schema.org/Organization) with a 
[ContactPoint](https://schema.org/contactPoint):

```json
{ 
  "@context" : "https://schema.org",
  "@type" : "Organization",
  "name" : "Example Co",
  "contactPoint" : [
    { "@type" : "ContactPoint",
      "telephone" : "+1-555-123-456",
      "contactType" : "support",
      "areaServed" : "US"
    } 
  ] 
}
```

And in another, it's rendered in a custom format for a database import:

```
{
  "import_org_name" : "Example Co",
  "import_tel" : "+1555123456"
}
```

For more information get into the [detail of output templating](#output-templating-detailed).

### Comparison to HyperText

HyperText, the HyperText Transfer Protocol (HTTP) and HyperText Markup Language (HTML) have 
changed the world. HyperData does not claim to be worthy of comparison to HyperText, 
but for some readers comparison can aid understanding:

| | HyperText | HyperData |
|-|-|-|
| **Target** | Humans | Machines |
| **Language** | HTML | Any data serialisation format (e.g. JSON) |
| **Scripting** | JavaScript | [Data Transformation Language (DTL)](https://www.getdtl.org) |
| **Client** | Web Browser | HyperData client |

## What is HyperData not?

HyperData is not:

- An effort to make all the data on the web, its meaning and interlinking relationships 
machine-readable (the Semantic Web) 
- A way to link resources together in an API
([HAL](https://github.com/mikekelly/hal_specification/blob/master/hal_specification.md) is designed for that)

## Detail

### Linking Objects (Detailed)

In the summary, we looked at a basic linked object:
```json
{
  "foo" : "bar",
  "@L" : {
    "href" : "example.json",
    "name" : "Example Object"
  }
}
```

The hyperlink (value of `@L` key) may be a `map` with the following keys:

- `href` – a `string` containing a URI of the target object. Required.
- `name` – a `string` specifying the name for the link, may be displayed to humans. Optional.
- `transclusion` - a `boolean` value that may influence the HyperData client in whether a referenced 
object is [transcluded](https://en.wikipedia.org/wiki/Transclusion) or not. Optional.

The hyperlink may also be a string where the string is considered the `href`:
```json
{
  "foo" : "bar",
  "@L" : "example.json"
}
```

A hyperlink is always expanded to a map in the output object.


#### Transclusion

HyperData clients may use [transclusion](https://en.wikipedia.org/wiki/Transclusion) to expand
HyperData objects by resolving all hypermedia references within the object. 

The following keys may be added to a hyperlink after transclusion:

- `object` – the `map` representing the resolved referenced object
- `errors` – an `array` of errors encountered in the transclusion process, with each `array item` 
consisting a `map` containing and error `code` and `description` of errors during transclusion

An example of an expanded
object can be seen below:

```json
{
  "a" : "alpha",
  "@L" : {
    "href" : "example.json",
    "name" : "Example Object",
    "object" : {
      "b" : "bravo"
    }
  }
}
```

A failed transclusion might result in the following object:

```json
{
  "a" : "alpha",
  "@L" : {
    "href" : "example.json",
    "name" : "Example Object",
    "errors" : [
      {
        "code" : "404",
        "description" : "HyperData object at https://hyperdata.uk/example.json not found."
      }
    ]
  }
}
```

### Scripting (Detailed)

HyperData clients accept input data, this data remains on the client and is not sent to a
HyperData server. A HyperData Object can reference input data using [DTL](https://www.getdtl.org)
and use conditional expressions to output different objects based on input data.

For example, the input data could represent a user's country and language:

```json
{
  "country" : "GB",
  "language" : "EN"
}
```

And a HyperObject can output different data conditionally, based on this input data:

```json
{
  "@n" : 1,
  "name" : "Example Co",
  "contact_methods" : [
    {
      "type" : "telephone",
      "detail" : "{ ?( $country == 'US' '+1555123456' ?( ?( $country == 'GB' '+441234567890' ?( $country == 'DE' '+491234567890' ) ) ) ) }",
      "label" : "{ ?( $language == 'DE' 'Kundendienst' 'Customer Support' ) }"
    }
  ]
}
```

Notice that in HyperData Objects, conditional expressions (and all other 
[DTL expressions](https://gitlab.com/jk0ne/DTL/-/blob/master/docs/DTL-Expressions.md)) are contained 
with a `"{braced string}"`, they can also be contained within standard DTL `"(: happy tags :)"`, e.g:

```json
{
  "@n" : 1,
  "name" : "Example Co",
  "contact_methods" : [
    {
      "type" : "telephone",
      "detail" : "(: ?( $country == 'US' '+1555123456' ?( ?( $country == 'GB' '+441234567890' ?( $country == 'DE' '+491234567890' ) ) ) ) :)",
      "label" : "(: ?( $language == 'DE' 'Kundendienst' 'Customer Support' ) :)"
    }
  ]
}
```


### Output Templating (Detailed)
Once the HyperData Object has been transformed by any embedded scripting, the output object can be
transformed for output templating. This output template is determined by the HyperData client or its user.

TODO: Examples
